/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

import com.atlassian.crowd.model.group.Group;

/**
 * Thrown to indicate an invalid model group.
 */
public class InvalidGroupException extends CrowdException
{
    private final Group group;

    /**
     * Constructs a new <code>InvalidGroupException</code> with the invalid group given and a cause.
     *
     * @param group the invalid group
     * @param cause the cause (a null value is permitted)
     */
    public InvalidGroupException(Group group, Throwable cause)
    {
        super(cause);
        this.group = group;
    }

    /**
     * Constructs a new <code>InvalidGroupException</code> with the invalid group and
     *
     * @param group invalid group
     * @param message detail message
     */
    public InvalidGroupException(Group group, String message)
    {
        super(message);
        this.group = group;
    }

    public InvalidGroupException(Group group, String message, Throwable cause)
    {
        super(message, cause);
        this.group = group;
    }

    public Group getGroup()
    {
        return group;
    }
}
