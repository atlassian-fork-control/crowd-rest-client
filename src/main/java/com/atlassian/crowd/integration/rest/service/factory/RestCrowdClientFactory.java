/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.service.factory;

import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/**
 * Factory class for creating a new instance of CrowdClient using REST.
 */
public class RestCrowdClientFactory implements CrowdClientFactory
{
    public CrowdClient newInstance(final String url, final String applicationName, final String applicationPassword)
    {
        final ClientProperties clientProperties = new RestClientProperties(url, applicationName, applicationPassword);

        return newInstance(clientProperties);
    }

    public CrowdClient newInstance(final ClientProperties clientProperties)
    {
        return new RestCrowdClient(clientProperties);
    }

    /**
     * This class is used for forcing the use of specified url, application
     * name and application password. Values in ClientPropertiesImpl can
     * be overridden using system properties.
     */
    private static class RestClientProperties extends ClientPropertiesImpl
    {
        private final String baseURL;
        private final String applicationName;
        private final String applicationPassword;

        RestClientProperties(final String url, final String applicationName, final String applicationPassword)
        {
            this.baseURL = StringUtils.removeEnd(url, "/");
            this.applicationName = applicationName;
            this.applicationPassword = applicationPassword;
            updateProperties(new Properties());
        }

        @Override
        public String getBaseURL()
        {
            return baseURL;
        }

        @Override
        public String getApplicationName()
        {
            return applicationName;
        }

        @Override
        public String getApplicationPassword()
        {
            return applicationPassword;
        }
    }
}
