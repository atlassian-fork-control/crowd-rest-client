/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.util;

import java.util.concurrent.ExecutionException;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Caches the instances of JAXBContext for each entity class for performance reasons
 * <a href="https://jaxb.java.net/guide/Performance_and_thread_safety.html">as suggested by the JAXB docs</a>.
 * Note that we cache JAXBContext instances because they are thread-safe, but not Marshallers/Unmarshallers because
 * they are not thread-safe.
 * For an extra performance gain, we could have a pool of (un)marshallers in the style of JAXBStringReaderProviders
 * in Jersey Client.
 */
public class JAXBContextCache
{
    private final LoadingCache<Class<?>, JAXBContext> cache =
            CacheBuilder.newBuilder().softValues().build(new CacheLoader<Class<?>, JAXBContext>()
            {
                @Override
                public JAXBContext load(Class<?> clazz) throws JAXBException
                {
                    return JAXBContext.newInstance(clazz);
                }
            });

    /**
     * @param clazz class for which a JAXBContext is requested
     * @return a thread-safe JAXBContext for the given class
     */
    public JAXBContext getJAXBContext(Class<?> clazz)
    {
        try
        {
            return cache.get(clazz);
        }
        catch (ExecutionException e)
        {
            throw new DataBindingException("Cannot instantiate JAXBContext for class " + clazz, e);
        }
    }
}
